const fs = require('fs');
const path = require('path');

// rename all files in a directory with 1 - 1000 and encode them with base64Url

// generate random base64Url string
const randomString = () => {
    const random = Math.random().toString(36).substring(2, 5) + Math.random().toString(36).substring(2, 5);
    return Buffer.from(random).toString('base64').replace(/=/g, '').replace(/\+/g, '-').replace(/\//g, '_');
};


const dir = './anal';
const files = fs.readdirSync(dir);
files.forEach((file, index) => {
    // ignore txt files
    if (file.includes('.txt')) return;
    const oldPath = path.join(dir, file);
    const newPath = path.join(dir, `${index + 1}_${randomString()}.gif`);
    fs.renameSync(oldPath, newPath);
    });


